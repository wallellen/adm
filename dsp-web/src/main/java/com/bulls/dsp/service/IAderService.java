package com.bulls.dsp.service;


import com.bulls.dsp.common.PageInfo;
import com.bulls.dsp.bean.mybatis.Ader;

import java.util.Date;

public interface IAderService {

    PageInfo<?> findAllEnableByIfNotNull(String name, int page, int size);

    void insertOrUpdate(Ader ader);

    void disableByIds(String ids);

    Float getBalance(Long adId, Date until);
}
