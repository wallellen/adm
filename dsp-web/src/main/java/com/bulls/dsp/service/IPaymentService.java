package com.bulls.dsp.service;

import com.bulls.dsp.common.PageInfo;
import com.bulls.dsp.bean.mybatis.Payment;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.Date;
import java.util.List;

public interface IPaymentService {

    List<Payment> findAllByAderIdGroupByDate(Long aderId);

    PageInfo<?> findByAderIdGroupByDate(Long aderId);

    void insert(Payment payment);

    Workbook exportExcel(Long aderId, Date start, Date end);
}
