package com.bulls.dsp.service;

import com.bulls.dsp.bean.mybatis.Aid;
import com.bulls.dsp.common.PageInfo;

import java.util.List;
import java.util.Map;

public interface IAidService {

    void insert(Aid aid);

    PageInfo<?> findAllByIfNotNull(String name, int currentPage, int pageSize);

    Map<String, List<List<String>>> getEnumsList();
}
