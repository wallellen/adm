package com.bulls.dsp.service;


import com.bulls.dsp.bean.mybatis.AidTask;
import com.bulls.dsp.common.PageInfo;

public interface IAidTaskService {

    void insert(AidTask aidTask);

    PageInfo<?> findAllByIfNotNull(String name, String aidType, int currentPage, int pageSize);
}
