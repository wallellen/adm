package com.bulls.dsp.service.impl;

import com.bulls.dsp.bean.CurrentUser;
import com.bulls.dsp.service.ILoginService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class LoginServiceImpl implements ILoginService{

    @Override
    public CurrentUser getCurrentUser() {

        CurrentUser currentUser = new CurrentUser();
        currentUser.setName("tongxu");
        currentUser.setNotifyCount(17);
        currentUser.setUserId(1000000L);
        return currentUser;
    }
}
