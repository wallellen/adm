package com.bulls.dsp.service;

import com.bulls.dsp.common.PageInfo;
import org.apache.poi.ss.usermodel.Workbook;

public interface IAdReportService {

    PageInfo<?> findAdReportDTO4NameByIfNotNull(String aderName, String orderName, String adName,
                                                String start, String end, int page, int size);

    PageInfo<?> findAdReportDTO4DateByIfNotNull(String name, int page, int size);

    Workbook exportExcelBaseDate(String name);

    Workbook exportExcelBaseName(String aderName, String orderName, String adName);
}
