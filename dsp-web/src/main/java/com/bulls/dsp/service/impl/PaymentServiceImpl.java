package com.bulls.dsp.service.impl;

import com.bulls.dsp.common.PageInfo;
import com.bulls.dsp.common.util.DateHelper;
import com.bulls.dsp.common.util.ExcelHelper;
import com.bulls.dsp.mapper.PaymentMapper;
import com.bulls.dsp.bean.excel.PaymentExcel;
import com.bulls.dsp.bean.enums.PaymentType;
import com.bulls.dsp.bean.mybatis.Payment;
import com.bulls.dsp.service.IAderService;
import com.bulls.dsp.service.IPaymentService;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class PaymentServiceImpl implements IPaymentService {

    @Resource
    private IAderService aderService;

    @Resource
    private PaymentMapper paymentMapper;

    @Override
    public void insert(Payment payment) {
        paymentMapper.insert(payment);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Payment> findAllByAderIdGroupByDate(Long aderId) {
        List<Payment> payments = paymentMapper.getAllByAderId(aderId,
                null, null, null);

        return this.toValidPaymentResultList(payments);
    }

    @Override
    @Transactional(readOnly = true)
    public PageInfo<?> findByAderIdGroupByDate(Long aderId) {

        return PageInfo.builder()
                .content(this.findAllByAderIdGroupByDate(aderId))
                .build();
    }

    private List<Payment> toValidPaymentResultList(List<Payment> payments) {
        List<Payment> dtos = new ArrayList<>();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        // TODO 全量拉取改成按类型分两次拉取
        for (Payment payment : payments) {
            String date = format.format(payment.getCreateAt());
            boolean isNew = true;
            for (Payment dto : dtos) {
                if (dto.getDate().equals(date)) {
                    if (payment.getPaymentType() == PaymentType.EXPEND.getValue()) {
                        dto.setDayCost(dto.getDayCost() + payment.getAmount());
                    } else if (payment.getPaymentType() == PaymentType.INCOME.getValue()) {
                        dto.setDayIncome(dto.getDayIncome() + payment.getAmount());
                    }
                    isNew = false;
                }
            }
            if (isNew) {
                Payment dto = new Payment();
                dto.setDate(date);
                if (payment.getPaymentType() == PaymentType.EXPEND.getValue()) {
                    dto.setDayCost(payment.getAmount());
                } else if (payment.getPaymentType() == PaymentType.INCOME.getValue()) {
                    dto.setDayIncome(payment.getAmount());
                }
                // TODO
                dto.setBalance(
                        aderService.getBalance(payment.getAderId(),
                                DateHelper.getDayEndTime(payment.getCreateAt()))
                );
                dtos.add(dto);
            }
        }
        return dtos;
    }

    @Override
    public Workbook exportExcel(Long aderId, Date start, Date end) {

        List<Payment> dtos = findAllByAderIdGroupByDate(aderId);

        String[] headers = new String[]{"日期", "消耗", "充值", "余额"};
        String sheetName = "收支明细";

        return ExcelHelper.createWorkBook(sheetName, headers, toPaymentExcel(dtos));
    }

    private List<PaymentExcel> toPaymentExcel(List<Payment> dtos) {
        List<PaymentExcel> payments = new ArrayList<>();
        for (Payment dto : dtos) {
            PaymentExcel excel = new PaymentExcel();
            BeanUtils.copyProperties(dto, excel);
            payments.add(excel);
        }

        return payments;
    }
}
