package com.bulls.dsp.service;

import com.bulls.dsp.bean.CurrentUser;

public interface ILoginService {

    CurrentUser getCurrentUser();
}
