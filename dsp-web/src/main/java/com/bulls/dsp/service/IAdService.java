package com.bulls.dsp.service;

import com.bulls.dsp.common.PageInfo;
import com.bulls.dsp.bean.mybatis.Advert;
import com.qiniu.api.auth.AuthException;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface IAdService {

    void insertOrUpdate(Advert advert);

    PageInfo<?> findAllByIfNotNull(String name, int page, int size);

    Map<String, List<List<String>>> getEnumsList();

    void changeAdStatus(Long id, String statusTo);

    void upload(MultipartFile multipartFile) throws AuthException;

}
