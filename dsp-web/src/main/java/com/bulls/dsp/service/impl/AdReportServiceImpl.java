package com.bulls.dsp.service.impl;

import com.bulls.dsp.common.PageInfo;
import com.bulls.dsp.common.util.DateHelper;
import com.bulls.dsp.common.util.ExcelHelper;
import com.bulls.dsp.mapper.AdReportMapper;
import com.bulls.dsp.bean.excel.AdReport4DateExcel;
import com.bulls.dsp.bean.excel.AdReport4NameExcel;
import com.bulls.dsp.bean.mybatis.AdReport;
import com.bulls.dsp.service.IAdReportService;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
@Transactional(readOnly = true)
public class AdReportServiceImpl implements IAdReportService {

    @Resource
    private AdReportMapper adReportMapper;

    @Override
    public PageInfo<?> findAdReportDTO4NameByIfNotNull(String aderName, String orderName, String adName,
                                                       String start, String end, int page, int size) {
        List<AdReport> adReport =
                adReportMapper.getNameReportBy(aderName, orderName, adName, start, processEndDate(end), page, size);
        int total = adReportMapper.getNameReportCount(aderName, orderName, adName, start, processEndDate(end));

//        PageInfo<AdReport> pageInfo = new PageInfo<>();
//        pageInfo.setContent(adReport);
//        pageInfo.setNumber(page);
//        pageInfo.setSize(size);
//        pageInfo.setTotalElements((long) total);
//        return pageInfo;
        return PageInfo.builder()
                .totalElements(total)
                .number(page)
                .size(size)
                .content(adReport)
                .build();
    }

    private String processEndDate(String date) {
        if (!StringUtils.isEmpty(date)) {
            Date end = DateHelper.string2Date(date);
            Date next = DateHelper.getDateAfter(end, 1);
            return DateHelper.time2DateString(next);
        }
        return date;
    }

    @Override
    public PageInfo<?> findAdReportDTO4DateByIfNotNull(String anyName, int page, int size) {
        List<AdReport> adReport = adReportMapper.getDateReportBy(anyName, page, size);

        return PageInfo.builder()
                .totalElements(adReportMapper.getDateReportCount(anyName))
                .number(page)
                .size(size)
                .content(adReport)
                .build();
    }

    @Override
    public Workbook exportExcelBaseDate(String anyName) {
        List<AdReport> adReport = adReportMapper.getDateReportBy(anyName, 1, 1000000);
        String[] headers = new String[]{"时间", "曝光PV", "曝光UV", "点击PV", "点击UV", "花费", "CTR", "CPM", "	CPC"};
        String sheetName = "广告数据报表";
        return ExcelHelper.createWorkBook(sheetName, headers, toDateExcelList(adReport));
    }

    @Override
    public Workbook exportExcelBaseName(String aderName, String orderName, String adName) {
        aderName = Objects.equals(aderName, "") ? null : aderName;
        orderName = Objects.equals(orderName, "") ? null : orderName;
        adName = Objects.equals(adName, "") ? null : adName;
        List<AdReport> adReport = adReportMapper.getNameReportBy(aderName, orderName, adName,
                null, null, 1, 1000000);
        String[] headers = new String[]{"名称", "曝光PV", "曝光UV", "点击PV", "点击UV", "花费", "CTR", "CPM", "	CPC"};
        String sheetName = "广告数据报表";
        return ExcelHelper.createWorkBook(sheetName, headers, toNameExcelList(adReport));
    }

    private List<AdReport4DateExcel> toDateExcelList(List<AdReport> adReports) {
        List<AdReport4DateExcel> excels = new ArrayList<>();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        for (AdReport adReport : adReports) {
            AdReport4DateExcel excel = new AdReport4DateExcel();
            BeanUtils.copyProperties(adReport, excel);
            excel.setCreateAt(format.format(adReport.getCreateAt()));
            excels.add(excel);
        }
        return excels;
    }

    private List<AdReport4NameExcel> toNameExcelList(List<AdReport> adReports) {
        List<AdReport4NameExcel> excels = new ArrayList<>();
        for (AdReport adReport : adReports) {
            AdReport4NameExcel excel = new AdReport4NameExcel();
            BeanUtils.copyProperties(adReport, excel);
            excels.add(excel);
        }
        return excels;
    }
}
