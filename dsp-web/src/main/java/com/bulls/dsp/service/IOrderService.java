package com.bulls.dsp.service;

import com.bulls.dsp.common.PageInfo;
import com.bulls.dsp.bean.mybatis.Order;

import java.util.List;
import java.util.Map;

public interface IOrderService {

//    Order saveOrUpdate(OrderDTO order);

    void insertOrUpdate(Order order);

    PageInfo<?> findAllByIfNotNull(String name, int page, int size);

    Map<String, List<List<String>>> getEnumsList();

    void changeOrderStatus(Long id, String statusTo);

}
