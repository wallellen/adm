package com.bulls.dsp.mapper;

import com.bulls.dsp.bean.mybatis.AdReport;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AdReportMapper {
    List<AdReport> getNameReportBy(@Param("aderName") String aderName,
                                   @Param("orderName") String orderName,
                                   @Param("adName") String adName,
                                   @Param("startDate") String startDate,
                                   @Param("endDate") String endDate,
                                   @Param("page") int page,
                                   @Param("size") int size);

    List<AdReport> getDateReportBy(@Param("name") String anyName,
                                   @Param("page") int page,
                                   @Param("size") int size);

    int getNameReportCount(@Param("aderName") String aderName,
                           @Param("orderName") String orderName,
                           @Param("adName") String adName,
                           @Param("startDate") String startDate,
                           @Param("endDate") String endDate);

    int getDateReportCount(@Param("name") String anyName);
}
