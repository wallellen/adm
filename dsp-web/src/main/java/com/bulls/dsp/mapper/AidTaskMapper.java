package com.bulls.dsp.mapper;

import com.bulls.dsp.bean.mybatis.AidTask;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AidTaskMapper {
    List<AidTask> getAidTasks(@Param("name") String name,
                              @Param("aidType") String aidType,
                              @Param("page") int page,
                              @Param("size") int size);

    int getAidTasksCount(@Param("name") String name,
                         @Param("aidType") String aidType);

    void insert(AidTask aidTask);
//
//    void update(Ader ader);
//
//    void disableAder(Long id);
//
//    Ader getAderByName(String name);
}
