package com.bulls.dsp.mapper;

import com.bulls.dsp.bean.mybatis.Aid;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AidMapper {
    List<Aid> getAids(@Param("name") String name,
                      @Param("page") int page,
                      @Param("size") int size);

    int getAidCount(@Param("name") String name);

    void insert(Aid aid);

    void update(Aid aid);

    Aid getAidByName(@Param("name") String name);
}
