package com.bulls.dsp.mapper;

import com.bulls.dsp.bean.mybatis.Slot;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SlotMapper {

    List<Slot> getSlots();

    Slot getSlotByName(String name);

}
