package com.bulls.dsp.mapper;

import com.bulls.dsp.bean.mybatis.Advert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AdvertMapper {

    List<Advert> getAdverts(@Param("name") String name,
                            @Param("page") int page,
                            @Param("size") int size);

    int getAdvertsCount(@Param("name") String name);

    void insert(Advert advert);

    void update(Advert advert);

    Advert getAdvertByName(String name);

    void updateStatus(Advert advert);

}
