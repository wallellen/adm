package com.bulls.dsp.mapper;

import com.bulls.dsp.bean.mybatis.Payment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PaymentMapper {
    //    List<Ader> getAders(@Param("name") String name,
//                         @Param("page") int page,
//                         @Param("size") int size);
    List<Payment> getAllByAderId(@Param("aderId") Long aderId,
                                 @Param("paymentType") String paymentType,
                                 @Param("startDate") String startDate,
                                 @Param("endDate") String endDate);

    List<Payment> getAllGroupByDateByAderId(@Param("aderId") Long aderId);

//    int getAderCountBy(@Param("name") String name);
//
    void insert(Payment payment);
//
//    void update(Ader ader);
//
//    void disableAder(Long id);
//
//    Ader getAderByName(String name);
}
