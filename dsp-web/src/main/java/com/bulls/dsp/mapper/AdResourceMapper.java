package com.bulls.dsp.mapper;

import com.bulls.dsp.bean.mybatis.AdResource;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AdResourceMapper {

    List<AdResource> getByAdId(Long adId);

    AdResource getByResKey(String name);

    void delete(Integer id);

    void fillAdId(AdResource adResource);

    void insert(AdResource adResource);
}
