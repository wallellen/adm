package com.bulls.dsp.mapper;

import com.bulls.dsp.bean.mybatis.Ader;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AderMapper {
    List<Ader> getAders(@Param("name") String name,
                        @Param("page") int page,
                        @Param("size") int size);

    int getAderCountBy(@Param("name") String name);

    void insert(Ader ader);

    void update(Ader ader);

    void disableAder(Long id);

    Ader getAderByName(String name);
}
