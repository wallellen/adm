package com.bulls.dsp.mapper;

import com.bulls.dsp.bean.mybatis.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderMapper {

    List<Order> getOrders(@Param("name") String name,
                          @Param("page") int page,
                          @Param("size") int size);

    int getOrdersCount(@Param("name") String name);

    void insert(Order order);

    void update(Order order);

    Order getOrderByName(String name);

    void updateStatus(Order order);

}
