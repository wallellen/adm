package com.bulls.dsp.controller;

import com.bulls.dsp.common.Response;
import com.bulls.dsp.service.IAdReportService;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

@RestController
@RequestMapping("/back_end/adReport")
public class AdReportController {

    @Resource
    private IAdReportService adReportService;

    @RequestMapping(value = "/all/byName", method = RequestMethod.GET)
    public Response<?> getByName(@RequestParam(required = false) String aderName,
                                 @RequestParam(required = false) String orderName,
                                 @RequestParam(required = false) String adName,
                                 @RequestParam(required = false) String startDate,
                                 @RequestParam(required = false) String endDate,
                                 @RequestParam(defaultValue = "1") int currentPage,
                                 @RequestParam(defaultValue = "10") int pageSize) {
        return Response.builder()
                .status(HttpStatus.OK.value())
                .data(adReportService.findAdReportDTO4NameByIfNotNull(aderName, orderName, adName, startDate, endDate, currentPage, pageSize))
                .build();
    }

    @RequestMapping(value = "/all/byDate", method = RequestMethod.GET)
    public Response<?> getByDate(@RequestParam String name,
                                 @RequestParam(defaultValue = "1") int currentPage,
                                 @RequestParam(defaultValue = "10") int pageSize) {
        return Response.builder()
                .status(HttpStatus.OK.value())
                .data(adReportService.findAdReportDTO4DateByIfNotNull(name, currentPage, pageSize))
                .build();
    }

    @RequestMapping(value = "/all/byDate/excel", method = RequestMethod.GET)
    public void getByDate(@RequestParam String name, HttpServletResponse response) throws IOException {

        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-disposition", "excel");
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename=" + "excel");
        response.setHeader("Pragma", "No-cache");

        OutputStream outputStream = response.getOutputStream();
        Workbook workbook = adReportService.exportExcelBaseDate(name);
        workbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
    }

    @RequestMapping(value = "/all/byName/excel", method = RequestMethod.GET)
    public void getByDate(@RequestParam(required = false) String aderName,
                          @RequestParam(required = false) String orderName,
                          @RequestParam(required = false) String adName,
                          HttpServletResponse response) throws IOException {

        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-disposition", "excel");
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename=" + "excel");
        response.setHeader("Pragma", "No-cache");

        OutputStream outputStream = response.getOutputStream();
        Workbook workbook = adReportService.exportExcelBaseName(aderName, orderName, adName);
        workbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
    }
}
