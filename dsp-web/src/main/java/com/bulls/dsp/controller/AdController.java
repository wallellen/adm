package com.bulls.dsp.controller;

import com.bulls.dsp.common.Response;
import com.bulls.dsp.bean.mybatis.Advert;
import com.bulls.dsp.service.IAdService;
import com.qiniu.api.auth.AuthException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@RestController
@RequestMapping("/back_end/ad")
public class AdController {

    @Resource
    private IAdService adService;

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public Response<?> upload(@RequestParam("file") MultipartFile file) throws AuthException {
        adService.upload(file);
        return Response.builder()
                .status(HttpStatus.OK.value())
                .build();
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public Response<?> getAll(@RequestParam(required = false) String name,
                              @RequestParam(name = "pageSize", defaultValue = "1") int currentPage,
                              @RequestParam(name = "currentPage", defaultValue = "10") int pageSize) {
        return Response.builder()
                .status(HttpStatus.OK.value())
                .data(adService.findAllByIfNotNull(name, currentPage, pageSize))
                .build();
    }

    @RequestMapping(value = "/enums", method = RequestMethod.GET)
    public Response<?> getEnums() {
        return Response.builder()
                .status(HttpStatus.OK.value())
                .data(adService.getEnumsList())
                .build();
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Response<?> saveOrUpdate(@RequestBody Advert advert) {
        adService.insertOrUpdate(advert);
        return Response.builder()
                .status(HttpStatus.OK.value())
                .build();

    }


    @RequestMapping(value = "/change", method = RequestMethod.GET)
    public Response<?> changeStatus(@RequestParam Long id, @RequestParam String adStatus) {
        adService.changeAdStatus(id, adStatus);

        return Response.builder()
                .status(HttpStatus.OK.value())
                .build();
    }

    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public Response<?> remove(@RequestBody String id) {
        return Response.builder()
                .status(HttpStatus.OK.value())
                .data(null)
                .build();
    }
}
