package com.bulls.dsp.controller;

import com.bulls.dsp.bean.mybatis.AidTask;
import com.bulls.dsp.common.Response;
import com.bulls.dsp.service.IAidTaskService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/back_end/aidTask")
public class AidTaskController {

    @Resource
    private IAidTaskService aidTaskService;

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Response<?> save(@RequestBody AidTask aidTask) {
        aidTaskService.insert(aidTask);
        return Response.builder()
                .status(HttpStatus.OK.value())
                .build();

    }

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public Response<?> getBy(@RequestParam(required = false) String name,
                             @RequestParam(required = false) String aidType,
                             @RequestParam(defaultValue = "1") int pageSize,
                             @RequestParam(defaultValue = "10") int currentPage) {
        return Response.builder()
                .status(HttpStatus.OK.value())
                .data(aidTaskService.findAllByIfNotNull(name, aidType, pageSize, currentPage))
                .build();
    }

//    @RequestMapping(value = "/enums", method = RequestMethod.GET)
//    public Response<?> getEnums() {
//        return Response.builder()
//                .status(HttpStatus.OK.value())
//                .data(orderService.getEnumsList())
//                .build();
//    }
//
//    @RequestMapping(value = "/change", method = RequestMethod.GET)
//    public Response<?> getByAdvertiserName(@RequestParam Long id, @RequestParam String orderStatus) {
//        return Response.builder()
//                .status(HttpStatus.OK.value())
//                .data(orderService.changeOrderStatus(id, orderStatus))
//                .build();
//    }


    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public Response<?> remove(@RequestBody String id) {
        return Response.builder()
                .status(HttpStatus.OK.value())
                .data(null)
                .build();
    }
}
