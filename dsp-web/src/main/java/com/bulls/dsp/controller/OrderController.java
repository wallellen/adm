package com.bulls.dsp.controller;

import com.bulls.dsp.common.Response;
import com.bulls.dsp.bean.mybatis.Order;
import com.bulls.dsp.service.IOrderService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/back_end/order")
public class OrderController {

    @Resource
    private IOrderService orderService;

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Response<?> save(@RequestBody Order order) {
        orderService.insertOrUpdate(order);
        return Response.builder()
                .status(HttpStatus.OK.value())
                .build();

    }

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public Response<?> getBy(@RequestParam(required = false) String name,
                             @RequestParam(defaultValue = "1") int pageSize,
                             @RequestParam(defaultValue = "10") int currentPage) {
        return Response.builder()
                .status(HttpStatus.OK.value())
                .data(orderService.findAllByIfNotNull(name, pageSize, currentPage))
                .build();
    }

    @RequestMapping(value = "/enums", method = RequestMethod.GET)
    public Response<?> getEnums() {
        return Response.builder()
                .status(HttpStatus.OK.value())
                .data(orderService.getEnumsList())
                .build();
    }

    @RequestMapping(value = "/change", method = RequestMethod.GET)
    public Response<?> getByAdvertiserName(@RequestParam Long id, @RequestParam String orderStatus) {
        orderService.changeOrderStatus(id, orderStatus);
        return Response.builder()
                .status(HttpStatus.OK.value())
                .build();
    }


    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public Response<?> remove(@RequestBody String ids) {
        return Response.builder()
                .status(HttpStatus.OK.value())
                .data(null)
                .build();
    }
}
