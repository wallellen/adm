package com.bulls.dsp.controller;

import com.bulls.dsp.common.Response;
import com.bulls.dsp.bean.Auth;
import com.bulls.dsp.service.ILoginService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/back_end/login")
public class LoginController {

    @Resource
    private ILoginService loginService;

    @RequestMapping(value = "/account", method = RequestMethod.POST)
    public Response<?> login(@RequestBody Auth auth) {
        return Response.builder()
                .data(new Auth("admin"))
                .status(HttpStatus.OK.value())
                .build();
    }

    @RequestMapping(value = "/currentUser", method = RequestMethod.GET)
    public Response<?> getCurrentUser() {
        return Response.builder()
                .status(HttpStatus.OK.value())
                .data(loginService.getCurrentUser())
                .build();
    }

}
