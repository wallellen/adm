package com.bulls.dsp.controller;

import com.bulls.dsp.common.Response;
import com.bulls.dsp.bean.mybatis.Ader;
import com.bulls.dsp.service.IAderService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/back_end/ader")
public class AderController {

    @Resource
    private IAderService aderService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public Response<?> getAll(@RequestParam(name = "name", required = false) String name,
                              @RequestParam(name = "currentPage", defaultValue = "1") int currentPage,
                              @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return Response.builder()
                .status(HttpStatus.OK.value())
                .data(aderService.findAllEnableByIfNotNull(name, currentPage, pageSize))
                .build();
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Response<?> saveOrUpdate(@RequestBody Ader ader) {
        aderService.insertOrUpdate(ader);
        return Response.builder()
                .status(HttpStatus.OK.value())
                .build();

    }

    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public Response<?> remove(@RequestBody String id) {
        aderService.disableByIds(id);
        return Response.builder()
                .status(HttpStatus.OK.value())
                .build();
    }

}
