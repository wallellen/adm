package com.bulls.dsp.controller;

import com.bulls.dsp.bean.mybatis.Aid;
import com.bulls.dsp.common.Response;
import com.bulls.dsp.service.IAidService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/back_end/aid")
public class AidController {

    @Resource
    private IAidService aidService;

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Response<?> save(@RequestBody Aid aid) {
        aidService.insert(aid);
        return Response.builder()
                .status(HttpStatus.OK.value())
                .build();

    }

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public Response<?> getBy(@RequestParam(required = false) String name,
                             @RequestParam(defaultValue = "1") int pageSize,
                             @RequestParam(defaultValue = "10") int currentPage) {
        return Response.builder()
                .status(HttpStatus.OK.value())
                .data(aidService.findAllByIfNotNull(name, pageSize, currentPage))
                .build();
    }

    @RequestMapping(value = "/enums", method = RequestMethod.GET)
    public Response<?> getEnums() {
        return Response.builder()
                .status(HttpStatus.OK.value())
                .data(aidService.getEnumsList())
                .build();
    }

    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public Response<?> remove(@RequestBody String id) {
        return Response.builder()
                .status(HttpStatus.OK.value())
                .data(null)
                .build();
    }
}
