package com.bulls.dsp.common;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PageInfo<T> {

    private T content;
    private long totalElements;
    private int size;
    private int number;
}
