package com.bulls.dsp.common;

import com.bulls.dsp.exception.*;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.qiniu.api.auth.AuthException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;

@Slf4j
@RestControllerAdvice(annotations = RestController.class)
public class ControllerAdvice {

    @ExceptionHandler(DataIntegrityViolationException.class)
    public Response<?> handleDataIntegrityViolationException(DataIntegrityViolationException e) {
        log.error("数据重复,或数据不完整", e);
        return Response.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message("数据重复,或数据不完整")
                .build();
    }

    @ExceptionHandler(DuplicateException.class)
    public Response<?> handleDuplicateException(DuplicateException e) {
        log.error(e.getMessage(), e);
        return Response.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message(e.getMessage())
                .build();
    }

    @ExceptionHandler(DateParseException.class)
    public Response<?> handleDateParseException(DateParseException e) {
        log.error("日期字符串解析异常", e);
        return Response.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message("日期字符串解析异常")
                .build();
    }

    @ExceptionHandler(ExcelException.class)
    public Response<?> handleExcelException(ExcelException e) {
        log.error("导出Excel失败", e);
        return Response.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message("导出Excel失败")
                .build();
    }

    @ExceptionHandler(IOException.class)
    public Response<?> handleIOException(IOException e) {
        log.error("IO 异常", e);
        return Response.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message("IO 异常")
                .build();
    }

    @ExceptionHandler(InvalidFormatException.class)
    public Response<?> handleInvalidFormatException(InvalidFormatException e) {
        log.error("參數不合法", e);
        return Response.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message("參數不合法")
                .build();
    }

    @ExceptionHandler(AuthException.class)
    public Response<?> handleAuthException(AuthException e) {
        log.error("七牛空间权限错误，请查看后重试", e);
        return Response.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message("七牛空间权限错误，请查看后重试")
                .build();
    }

    @ExceptionHandler(FileUploadIOException.class)
    public Response<?> handleFIleUploadIOException(FileUploadIOException e) {
        log.error("文件上传失败", e);
        return Response.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message("文件上传失败")
                .build();
    }

    @ExceptionHandler(UnknownTypeException.class)
    public Response<?> handleUnknownTypeException(UnknownTypeException e) {
        log.error("未知枚举类型", e);
        return Response.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message("未知枚举类型")
                .build();
    }


    @ExceptionHandler(NoSuchAderException.class)
    public Response<?> handleNoSuchAderException(NoSuchAderException e) {
        log.error("广告主不存在或已无效", e);
        return Response.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message("广告主不存在或已无效")
                .build();
    }

    @ExceptionHandler(NoSuchAdException.class)
    public Response<?> handleNoSuchAdException(NoSuchAdException e) {
        log.error("广告不存在", e);
        return Response.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message("广告不存在")
                .build();
    }

    @ExceptionHandler(NoSuchAidException.class)
    public Response<?> handleNoSuchAdiException(NoSuchAidException e) {
        log.error("效果渠道不存在", e);
        return Response.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message("效果渠道不存在")
                .build();
    }

    @ExceptionHandler(NoSuchOrderException.class)
    public Response<?> handleNoSuchOrderException(NoSuchOrderException e) {
        log.error("订单不存在", e);
        return Response.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message("订单不存在")
                .build();
    }

//    @ExceptionHandler(StaleObjectStateException.class)
//    public Response<?> handleStaleObjectStateException(StaleObjectStateException e) {
//        log.error("数据已被其他人修改", e);
//        return Response.builder()
//                .status(HttpStatus.BAD_REQUEST.value())
//                .message("数据已被其他人修改")
//                .build();
//    }


    @ExceptionHandler(Exception.class)
    public Response<?> handleException(Exception e) {
        log.error("未知异常", e);
        return Response.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message("未知异常")
                .build();
    }
}
