package com.bulls.dsp.common;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class Response<T> {
    private Integer status;
    private String message;
    private T data;
}
