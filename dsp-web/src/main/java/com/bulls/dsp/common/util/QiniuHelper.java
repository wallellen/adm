package com.bulls.dsp.common.util;

import com.bulls.dsp.common.Constant;
import com.qiniu.api.auth.AuthException;
import com.qiniu.api.auth.digest.Mac;
import com.qiniu.api.io.IoApi;
import com.qiniu.api.io.PutExtra;
import com.qiniu.api.io.PutRet;
import com.qiniu.api.rs.PutPolicy;

public class QiniuHelper {

    public static PutRet upload(String fileName) throws AuthException {
        Mac mac = new Mac(Constant.ACCESS_KEY, Constant.SECRET_KEY);
        PutPolicy putPolicy = new PutPolicy(Constant.BUCKET_NAME);
        String upToken = putPolicy.token(mac);
        PutExtra extra = new PutExtra();
        String localFile = "./" + fileName;
        return IoApi.putFile(upToken, fileName, localFile, extra);
    }
}
