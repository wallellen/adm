package com.bulls.dsp.bean.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public enum Pos {
    POS1(0, "位置1"),
    POS2(1, "位置2"),
    POS3(2, "位置3"),
    POS4(3, "位置4"),
    POS5(4, "位置5"),
    POS6(5, "位置6"),
    POS7(6, "位置7"),
    POS8(7, "位置8"),
    POS9(8, "位置9"),
    OTHER(9, "其他");

    @Getter
    private int value;
    @Getter
    private String type;

    public static Pos getByStringCode(String code) {
        for (Pos adStatus : Pos.values()) {
            if (code.equals(String.valueOf(adStatus.getValue()))) {
                return adStatus;
            }
        }
        return null;
    }


    public static Pos getByStringValue(String value) {
        for (Pos adStatus : Pos.values()) {
            if (value.equals(String.valueOf(adStatus.getType()))) {
                return adStatus;
            }
        }
        return null;
    }

    public static List<List<String>> getCodeValueList() {

        List<List<String>> result = new ArrayList<>();

        for (Pos adType : Pos.values()) {
            List<String> list = new ArrayList<>();
            list.add(String.valueOf(adType.getValue()));
            list.add(adType.getType());
            result.add(list);
        }

        return result;
    }
}
