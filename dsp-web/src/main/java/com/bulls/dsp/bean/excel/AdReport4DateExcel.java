package com.bulls.dsp.bean.excel;

import lombok.Data;

@Data
public class AdReport4DateExcel {
    private String createAt;
    private Integer exposePV;
    private Integer exposeUV;
    private Integer clickPV;
    private Integer clickUV;
    private Integer cost;
    private Integer ctr;
    private Integer cpm;
    private Integer cpc;
}

