package com.bulls.dsp.bean.mybatis;

import lombok.Data;

import java.util.Date;

@Data
public class Slot {
    private Integer id;

    private Date createAt;

    private Date updateAt;

    private String name;

    private String type;

    private Boolean status;

    private String remark;

    private String picSize;
}
