package com.bulls.dsp.bean.mybatis;

import lombok.Data;

import java.util.Date;

@Data
public class Payment {
    private Long id;
    private int paymentType;
    private Float amount;
    private Long aderId;
    private Date createAt;

    // for 收支明细
    private String date;
    private Float dayCost = 0.0f;
    private Float dayIncome = 0.0f;
    private Float balance = 0.0f;
}
