package com.bulls.dsp.bean.mybatis;

import lombok.Data;

import java.util.Date;

@Data
public class AidTask {
    private Long id;
    private String name;
    private String aidUrl;
    private String aderName;
    private String orderName;
    private String adName;
    private String aidName;
    private Long adId;
    private Long aidId;
    private Date createAt;
    private String aidType;
}
