package com.bulls.dsp.bean.excel;

import lombok.Data;

@Data
public class PaymentExcel {
    private String date;
    private Float dayCost = 0.0f;
    private Float dayIncome = 0.0f;
    private Float balance = 0.0f;
}
