package com.bulls.dsp.bean.mybatis;

import lombok.Data;

import java.util.Date;

@Data
public class AdReport {
    private String name;
    private Date createAt;
    private Integer exposePV;
    private Integer exposeUV;
    private Integer clickPV;
    private Integer clickUV;
    private Double cost;
    private Double ctr;
    private Double cpm;
    private Double cpc;
}
