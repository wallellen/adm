package com.bulls.dsp.bean.mybatis;

import lombok.Data;

import java.util.Date;

@Data
public class Ader {
    private Long id;
    private Date createAt;
    private String name;
    private String adUrl;
    private String phone;
    private Boolean enable;
    private String token;

    // 额外字段
    private Float balance;
    private Float costHistory;
    private Float costToday;
}
