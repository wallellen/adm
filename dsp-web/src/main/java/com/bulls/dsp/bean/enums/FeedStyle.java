package com.bulls.dsp.bean.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public enum FeedStyle {

    SINGLE(0, "单图样式"),
    MULI(1, "多图样式"),
    LARGE(2, "大图样式");

    @Getter
    private int value;
    @Getter
    private String type;

    public static FeedStyle getByStringCode(String code) {
        for (FeedStyle adStatus : FeedStyle.values()) {
            if (code.equals(String.valueOf(adStatus.getValue()))) {
                return adStatus;
            }
        }
        return null;
    }


    public static FeedStyle getByStringValue(String value) {
        for (FeedStyle adStatus : FeedStyle.values()) {
            if (value.equals(String.valueOf(adStatus.getType()))) {
                return adStatus;
            }
        }
        return null;
    }

    public static List<List<String>> getCodeValueList() {

        List<List<String>> result = new ArrayList<>();

        for (FeedStyle adType : FeedStyle.values()) {
            List<String> list = new ArrayList<>();
            list.add(String.valueOf(adType.getValue()));
            list.add(adType.getType());
            result.add(list);
        }

        return result;
    }
}
