package com.bulls.dsp.bean.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public enum FeedFrom {
    BULLS(0, "牛牛头条");

    @Getter
    private int value;
    @Getter
    private String type;

    public static FeedFrom getByStringCode(String code) {
        for (FeedFrom adStatus : FeedFrom.values()) {
            if (code.equals(String.valueOf(adStatus.getValue()))) {
                return adStatus;
            }
        }
        return null;
    }


    public static FeedFrom getByStringValue(String value) {
        for (FeedFrom adStatus : FeedFrom.values()) {
            if (value.equals(String.valueOf(adStatus.getType()))) {
                return adStatus;
            }
        }
        return null;
    }

    public static List<List<String>> getCodeValueList() {

        List<List<String>> result = new ArrayList<>();

        for (FeedFrom adType : FeedFrom.values()) {
            List<String> list = new ArrayList<>();
            list.add(String.valueOf(adType.getValue()));
            list.add(adType.getType());
            result.add(list);
        }

        return result;
    }
}
