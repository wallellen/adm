package com.bulls.dsp.bean.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public enum AidType {

    EXPOSURE(0, "曝光"),
    CLICK(1, "点击"),
    TRANSFORM(2, "转化");

    @Getter
    private int value;
    @Getter
    private String type;

    public static AidType getByStringCode(String code) {
        for (AidType adStatus : AidType.values()) {
            if (code.equals(String.valueOf(adStatus.getValue()))) {
                return adStatus;
            }
        }
        return null;
    }


    public static AidType getByStringValue(String value) {
        for (AidType adStatus : AidType.values()) {
            if (value.equals(String.valueOf(adStatus.getType()))) {
                return adStatus;
            }
        }
        return null;
    }

    public static List<List<String>> getCodeValueList() {

        List<List<String>> result = new ArrayList<>();

        for (AidType adType : AidType.values()) {
            List<String> list = new ArrayList<>();
            list.add(String.valueOf(adType.getValue()));
            list.add(adType.getType());
            result.add(list);
        }

        return result;
    }
}
