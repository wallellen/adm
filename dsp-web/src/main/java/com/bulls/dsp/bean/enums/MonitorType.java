package com.bulls.dsp.bean.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public enum MonitorType {

    MIAOZHEN(0, "秒针"),
    ADMATER(1, "AdMaster");

    @Getter
    private int value;
    @Getter
    private String type;

    public static MonitorType getByStringCode(String code) {
        for (MonitorType adStatus : MonitorType.values()) {
            if (code.equals(String.valueOf(adStatus.getValue()))) {
                return adStatus;
            }
        }
        return null;
    }


    public static MonitorType getByStringValue(String value) {
        for (MonitorType adStatus : MonitorType.values()) {
            if (value.equals(String.valueOf(adStatus.getType()))) {
                return adStatus;
            }
        }
        return null;
    }

    public static List<List<String>> getCodeValueList() {

        List<List<String>> result = new ArrayList<>();

        for (MonitorType adType : MonitorType.values()) {
            List<String> list = new ArrayList<>();
            list.add(String.valueOf(adType.getValue()));
            list.add(adType.getType());
            result.add(list);
        }

        return result;
    }
}
