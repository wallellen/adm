package com.bulls.dsp.bean.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum MediaType {
    BULLS(0, "牛牛头条"),
    AID(1, "效果渠道");

    @Getter
    private int value;
    @Getter
    private String type;

    public static MediaType getByStringCode(String code) {
        for (MediaType adStatus : MediaType.values()) {
            if (code.equals(String.valueOf(adStatus.getValue()))) {
                return adStatus;
            }
        }
        return null;
    }
}
