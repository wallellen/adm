package com.bulls.dsp.bean.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public enum PaymentType {

    INCOME(0, "充值"),
    EXPEND(1, "消耗");

    @Getter
    private int value;
    @Getter
    private String type;

    public static PaymentType getByStringCode(String code){
        for(PaymentType paymentType : PaymentType.values()){
            if(code.equals(String.valueOf(paymentType.getValue()))){
                return paymentType;
            }
        }
        return null;
    }


    public static PaymentType getByStringValue(String value) {
        for (PaymentType adStatus : PaymentType.values()) {
            if (value.equals(String.valueOf(adStatus.getType()))) {
                return adStatus;
            }
        }
        return null;
    }

    public static List<List<String>> getCodeValueList() {

        List<List<String>> result = new ArrayList<>();

        for (PaymentType adType : PaymentType.values()) {
            List<String> list = new ArrayList<>();
            list.add(String.valueOf(adType.getValue()));
            list.add(adType.getType());
            result.add(list);
        }

        return result;
    }
}
