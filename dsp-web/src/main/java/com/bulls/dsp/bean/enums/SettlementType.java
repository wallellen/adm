package com.bulls.dsp.bean.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public enum SettlementType {
    CPT(0, "CPT"),
    CPM(1, "CPM"),
    CPC(2, "CPC");

    @Getter
    private int value;
    @Getter
    private String type;

    public static SettlementType getByStringCode(String code) {
        for (SettlementType adStatus : SettlementType.values()) {
            if (code.equals(String.valueOf(adStatus.getValue()))) {
                return adStatus;
            }
        }
        return null;
    }


    public static SettlementType getByStringValue(String value) {
        for (SettlementType adStatus : SettlementType.values()) {
            if (value.equals(String.valueOf(adStatus.getType()))) {
                return adStatus;
            }
        }
        return null;
    }

    public static List<List<String>> getCodeValueList() {

        List<List<String>> result = new ArrayList<>();

        for (SettlementType adType : SettlementType.values()) {
            List<String> list = new ArrayList<>();
            list.add(String.valueOf(adType.getValue()));
            list.add(adType.getType());
            result.add(list);
        }

        return result;
    }
}
