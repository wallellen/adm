package com.bulls.dsp.bean.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public enum OrderStatus {

    TO_RELEASE(0, "未投放"),
    RELEASING(1, "投放中");

    @Getter
    private int value;
    @Getter
    private String status;

    public static OrderStatus getByStringCode(String code) {
        for (OrderStatus adStatus : OrderStatus.values()) {
            if (code.equals(String.valueOf(adStatus.getValue()))) {
                return adStatus;
            }
        }
        return null;
    }


    public static OrderStatus getByStringValue(String value) {
        for (OrderStatus adStatus : OrderStatus.values()) {
            if (value.equals(String.valueOf(adStatus.getStatus()))) {
                return adStatus;
            }
        }
        return null;
    }

    public static List<List<String>> getCodeValueList() {

        List<List<String>> result = new ArrayList<>();

        for (OrderStatus adType : OrderStatus.values()) {
            List<String> list = new ArrayList<>();
            list.add(String.valueOf(adType.getValue()));
            list.add(adType.getStatus());
            result.add(list);
        }

        return result;
    }
}
