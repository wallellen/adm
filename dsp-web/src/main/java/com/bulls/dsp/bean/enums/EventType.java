package com.bulls.dsp.bean.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public enum EventType {

    CLICK(0, "点击"),
    SHOW(1, "曝光");

    @Getter
    private int value;
    @Getter
    private String status;

    public static EventType getByStringCode(String code) {
        for (EventType adStatus : EventType.values()) {
            if (code.equals(String.valueOf(adStatus.getValue()))) {
                return adStatus;
            }
        }
        return null;
    }

    public static EventType getByStringValue(String value) {
        for (EventType adStatus : EventType.values()) {
            if (value.equals(String.valueOf(adStatus.getStatus()))) {
                return adStatus;
            }
        }
        return null;
    }


    public static List<List<String>> getCodeValueList() {

        List<List<String>> result = new ArrayList<>();

        for (EventType adType : EventType.values()) {
            List<String> list = new ArrayList<>();
            list.add(String.valueOf(adType.getValue()));
            list.add(adType.getStatus());
            result.add(list);
        }

        return result;
    }
}
