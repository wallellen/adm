package com.bulls.dsp.bean.mybatis;

import com.bulls.dsp.bean.FileInfo;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class Advert {

    private Long id;
    private String name;
    private String adStatus;
    private String slotName;
    private Date createAt;
    private Date startDate;
    private Date endDate;
    private List<Date> date;
    private Long orderId;
    private String orderName;

    private String adUrl;
    private String monitorType;
    private String exposeMonitor;
    private String clickMonitor;

    private List<FileInfo> fileInfos;

    private String aderName;

    // 投放量
    private Integer size;
    // 分享类型广告独有
    // 被浏览次数
    private Integer viewTime;
    // 最高收益
    private Float topGain;

    // 频道信息流类型独有
    // 信息流样式
    private String feedStyle;

    private String adTitle;

    private Float price;
}
