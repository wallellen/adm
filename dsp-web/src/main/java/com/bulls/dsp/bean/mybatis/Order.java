package com.bulls.dsp.bean.mybatis;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class Order {

    private Long id;
    private String name;
    private String slotName;
    private Integer slotId;
    private String settlementType;
    private List<Date> date;
    private Date createAt;
    private String orderStatus;
    private String carouselType;
    private Long aderId;
    private String aderName;

    private Date startDate;
    private Date endDate;

    private String channel;
    private String pos;
    private String picSize;

    private Integer size;
}
