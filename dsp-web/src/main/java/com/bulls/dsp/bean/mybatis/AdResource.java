package com.bulls.dsp.bean.mybatis;

import lombok.Data;

@Data
public class AdResource {
    private Integer id;
    private String resKey;
    private Long adId;
}
