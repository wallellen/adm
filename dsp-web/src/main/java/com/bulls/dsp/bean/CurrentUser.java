package com.bulls.dsp.bean;

import lombok.Data;

@Data
public class CurrentUser {
    private String name;
    private String avatar;
    private Long userId;
    private Integer notifyCount;
}
