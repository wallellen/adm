package com.bulls.dsp.bean;

import lombok.Data;

@Data
public class FileInfo {
    private String name;
    private String url;
    private String status;
    private String uid;
}
