package com.bulls.dsp.bean;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Auth {

    private String currentAuthority;
}
