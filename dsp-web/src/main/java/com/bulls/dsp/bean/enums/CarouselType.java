package com.bulls.dsp.bean.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public enum CarouselType {
    QUARTER(0, "1/4"),
    A_HALf(1, "1/2"),
    ALL(2, "独占");

    @Getter
    private int value;
    @Getter
    private String type;

    public static CarouselType getByStringCode(String code) {
        for (CarouselType adStatus : CarouselType.values()) {
            if (code.equals(String.valueOf(adStatus.getValue()))) {
                return adStatus;
            }
        }
        return null;
    }


    public static CarouselType getByStringValue(String value) {
        for (CarouselType adStatus : CarouselType.values()) {
            if (value.equals(String.valueOf(adStatus.getType()))) {
                return adStatus;
            }
        }
        return null;
    }

    public static List<List<String>> getCodeValueList() {

        List<List<String>> result = new ArrayList<>();

        for (CarouselType adType : CarouselType.values()) {
            List<String> list = new ArrayList<>();
            list.add(String.valueOf(adType.getValue()));
            list.add(adType.getType());
            result.add(list);
        }

        return result;
    }
}
