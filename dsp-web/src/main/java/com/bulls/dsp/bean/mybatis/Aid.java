package com.bulls.dsp.bean.mybatis;

import lombok.Data;

@Data
public class Aid {
    private Long id;
    private String name;
    private String aidType;
    private Boolean enable;
}
