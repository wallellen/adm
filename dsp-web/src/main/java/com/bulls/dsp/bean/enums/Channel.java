package com.bulls.dsp.bean.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public enum Channel {
    TOUTIAO(0, "头条"),
    GAOXIAO(1, "搞笑"),
    YULE(2, "娱乐"),
    TIYU(3, "体育"),
    JUNSHI(4, "军事"),
    KEJI(5, "科技"),
    CAIJING(6, "财经"),
    SHISHANF(7, "时尚"),
    YOUXI(8, "游戏"),
    QICHE(9, "汽车"),
    JIANKANG(10, "健康");

    @Getter
    private int value;
    @Getter
    private String type;

    public static Channel getByStringCode(String code) {
        for (Channel adStatus : Channel.values()) {
            if (code.equals(String.valueOf(adStatus.getValue()))) {
                return adStatus;
            }
        }
        return null;
    }


    public static Channel getByStringValue(String value) {
        for (Channel adStatus : Channel.values()) {
            if (value.equals(String.valueOf(adStatus.getType()))) {
                return adStatus;
            }
        }
        return null;
    }

    public static List<List<String>> getCodeValueList() {

        List<List<String>> result = new ArrayList<>();

        for (Channel adType : Channel.values()) {
            List<String> list = new ArrayList<>();
            list.add(String.valueOf(adType.getValue()));
            list.add(adType.getType());
            result.add(list);
        }

        return result;
    }
}
