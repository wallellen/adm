package com.bulls.dsp.bean.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public enum AdStatus {

    TO_RELEASE(0, "未投放"),
    RELEASING(1, "投放中");

    @Getter
    private int value;
    @Getter
    private String status;

    public static AdStatus getByStringCode(String code) {
        for (AdStatus adStatus : AdStatus.values()) {
            if (code.equals(String.valueOf(adStatus.getValue()))) {
                return adStatus;
            }
        }
        return null;
    }

    public static AdStatus getByStringValue(String value) {
        for (AdStatus adStatus : AdStatus.values()) {
            if (value.equals(String.valueOf(adStatus.getStatus()))) {
                return adStatus;
            }
        }
        return null;
    }


    public static List<List<String>> getCodeValueList() {

        List<List<String>> result = new ArrayList<>();

        for (AdStatus adType : AdStatus.values()) {
            List<String> list = new ArrayList<>();
            list.add(String.valueOf(adType.getValue()));
            list.add(adType.getStatus());
            result.add(list);
        }

        return result;
    }
}
