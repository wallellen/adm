package com.bulls.dsp.bean.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public enum PicSize {

    S300250(0, "300*250"),
    S96090(1, "960*90"),
    S336280(2, "336*280"),
    S250250(3, "250*250"),
    S72890(4, "728*90");

    @Getter
    private int value;
    @Getter
    private String type;

    public static PicSize getByStringCode(String code) {
        for (PicSize adStatus : PicSize.values()) {
            if (code.equals(String.valueOf(adStatus.getValue()))) {
                return adStatus;
            }
        }
        return null;
    }


    public static PicSize getByStringValue(String value) {
        for (PicSize adStatus : PicSize.values()) {
            if (value.equals(String.valueOf(adStatus.getType()))) {
                return adStatus;
            }
        }
        return null;
    }

    public static List<List<String>> getCodeValueList() {

        List<List<String>> result = new ArrayList<>();

        for (PicSize adType : PicSize.values()) {
            List<String> list = new ArrayList<>();
            list.add(String.valueOf(adType.getValue()));
            list.add(adType.getType());
            result.add(list);
        }

        return result;
    }
}
