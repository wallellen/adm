package com.bulls.dsp.exception;

public class UnknownTypeException extends RuntimeException {
    public UnknownTypeException(){
        super("未知类型");
    }
}
