package com.bulls.dsp.exception;

public class DateParseException extends RuntimeException {
    public DateParseException(String message) {
        super(message);
    }

    public DateParseException() {
    }
}
