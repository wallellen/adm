package com.bulls.dsp.exception;

public class ExcelException extends RuntimeException {
    public ExcelException(String message) {
        super(message);
    }
}
