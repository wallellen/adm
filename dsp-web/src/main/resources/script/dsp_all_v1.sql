CREATE TABLE `dsp_ader` (
  `id`        BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `create_at` DATETIME(6)           DEFAULT NULL,
  `modify_at` DATETIME(6)           DEFAULT NULL,
  `ad_url`    VARCHAR(255)          DEFAULT NULL,
  `enable`    BIT(1)                DEFAULT NULL,
  `name`      VARCHAR(255) NOT NULL,
  `phone`     VARCHAR(255)          DEFAULT NULL,
  `token`     VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_uk` (`token`),
  KEY `name_create_at_idx` (`name`, `create_at`)
)
  COMMENT '广告主'
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `dsp_ad_order` (
  `id`              BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `create_at`       DATETIME(6)           DEFAULT NULL,
  `modify_at`       DATETIME(6)           DEFAULT NULL,
  `carousel_type`   INT(11)               DEFAULT NULL
  COMMENT '轮播类型 只适用于CPT：0 1/4 , 1 1/2 , 2 独占',
  `channel`         INT(11)               DEFAULT NULL
  COMMENT '频道：0 头条, 1 搞笑，2 娱乐，3 体育，4 军事，5 科技，6 财经，7 时尚，8 游戏，9 汽车，10 健康',
  `end_date`        DATETIME(6)           DEFAULT NULL,
  `name`            VARCHAR(255) NOT NULL,
  `order_status`    INT(11)               DEFAULT NULL
  COMMENT '订单状态：0 投放中 1 未投放',
  `pic_size`        INT(11)               DEFAULT NULL
  COMMENT '图片大小：0 300*250, 1 960*90，2 336*280，3 250*250，4 728*90',
  `pos`             INT(11)               DEFAULT NULL
  COMMENT '位置：0 - 8 分别对应 位置1 到 位置9， 9 代表 其他',
  `settlement_type` INT(11)               DEFAULT NULL
  COMMENT '结算方式：0 CPT, 1 CPM, 2 CPC',
  `size`            INT(11)               DEFAULT NULL
  COMMENT '投放量，不适用于CPT',
  `slot_id`         INT(11)               DEFAULT NULL,
  `start_date`      DATETIME(6)           DEFAULT NULL,
  `ader_id`         BIGINT(20)            DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ader_id_create_at_idx` (`ader_id`, `create_at`),
  KEY `name_create_at_idx` (`name`, `create_at`)
)
  COMMENT '订单'
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `dsp_ad` (
  `id`             BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `create_at`      DATETIME(6)           DEFAULT NULL,
  `modify_at`      DATETIME(6)           DEFAULT NULL,
  `ad_status`      INT(11)               DEFAULT NULL
  COMMENT '广告状态: 0 投放中, 1 未投放',
  `ad_title`       VARCHAR(255)          DEFAULT NULL
  COMMENT '广告标题',
  `ad_url`         VARCHAR(255)          DEFAULT NULL,
  `click_monitor`  VARCHAR(255)          DEFAULT NULL,
  `end_date`       DATETIME(6)           DEFAULT NULL,
  `expose_monitor` VARCHAR(255)          DEFAULT NULL,
  `feed_from`      INT(11)               DEFAULT NULL
  COMMENT '流量来源，目前只有牛牛头条; 0 牛牛头条',
  `feed_style`     INT(11)               DEFAULT NULL
  COMMENT '信息流样式，频道信息流类型独有. 0 单图，1 多图，2 大图',
  `monitor_type`   INT(11)               DEFAULT NULL
  COMMENT '监控类型: 0 秒针, 1 AdMaster',
  `name`           VARCHAR(255) NOT NULL,
  `order_id`       BIGINT(20)   NOT NULL,
  `size`           INT(11)               DEFAULT NULL
  COMMENT '投放量',
  `start_date`     DATETIME(6)           DEFAULT NULL,
  `top_gain`       DECIMAL(18, 4)        DEFAULT NULL
  COMMENT '最高收益',
  `view_time`      INT(11)               DEFAULT NULL
  COMMENT '被浏览次数, 分享类型广告独有',
  PRIMARY KEY (`id`),
  KEY `order_id_create_at_idx` (`order_id`, `create_at`),
  KEY `name_create_at_idx` (`name`, `create_at`)
)
  COMMENT '广告'
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `dsp_ad_report` (
  `id`        BIGINT(20) NOT NULL AUTO_INCREMENT,
  `create_at` DATETIME(6)         DEFAULT NULL,
  `modify_at` DATETIME(6)         DEFAULT NULL,
  `ad_id`     BIGINT(20) NOT NULL,
  `clickpv`   INT(11)             DEFAULT NULL,
  `clickuv`   INT(11)             DEFAULT NULL,
  `cost`      DECIMAL(18, 4)      DEFAULT NULL,
  `cpc`       DECIMAL(18, 4)      DEFAULT NULL,
  `cpm`       DECIMAL(18, 4)      DEFAULT NULL,
  `ctr`       DECIMAL(18, 4)      DEFAULT NULL,
  `data_date` DATE                DEFAULT NULL,
  `exposepv`  INT(11)             DEFAULT NULL,
  `exposeuv`  INT(11)             DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ad_id_data_date_idx` (`ad_id`, `data_date`)
)
  COMMENT '数据报表'
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `dsp_ad_resource` (
  `id`        BIGINT(20) NOT NULL AUTO_INCREMENT,
  `create_at` DATETIME(6)         DEFAULT NULL,
  `modify_at` DATETIME(6)         DEFAULT NULL,
  `ad_id`     BIGINT(20)          DEFAULT NULL,
  `res_key`   VARCHAR(255)        DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `res_key_uk` (`res_key`)
)
  COMMENT '广告资源'
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `dsp_aid` (
  `id`        BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `create_at` DATETIME(6)           DEFAULT NULL,
  `modify_at` DATETIME(6)           DEFAULT NULL,
  `aid_type`  INT(11)               DEFAULT NULL
  COMMENT '效果渠道类型，0 曝光，1 点击，2 转化',
  `enable`    BIT(1)                DEFAULT NULL,
  `name`      VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name_create_at_idx` (`name`, `create_at`)
)
  COMMENT '效果渠道'
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `dsp_aid_task` (
  `id`        BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `create_at` DATETIME(6)           DEFAULT NULL,
  `modify_at` DATETIME(6)           DEFAULT NULL,
  `ad_id`     BIGINT(20)   NOT NULL,
  `aid_id`    BIGINT(20)   NOT NULL,
  `aid_url`   VARCHAR(255)          DEFAULT NULL,
  `name`      VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ad_id_create_at_idx` (`ad_id`, `create_at`),
  KEY `name_create_at_idx` (`name`, `create_at`)
)
  COMMENT '效果渠道任务'
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `dsp_payment` (
  `id`           BIGINT(20) NOT NULL AUTO_INCREMENT,
  `create_at`    DATETIME(6)         DEFAULT NULL,
  `modify_at`    DATETIME(6)         DEFAULT NULL,
  `ader_id`      BIGINT(20) NOT NULL,
  `amount`       FLOAT               DEFAULT NULL,
  `payment_type` INT(11)    NOT NULL
  COMMENT '收支类型，0 充值，1 消耗',
  PRIMARY KEY (`id`),
  KEY `ad_id_create_at_idx` (`ader_id`, `create_at`)
)
  COMMENT '收入支出明细'
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;



