#!/usr/bin/env bash

PROJECT_PATH=/home/ant/test

FE_HOME_PATH=${PROJECT_PATH}/adm-fe
BE_HOME_PATH=${PROJECT_PATH}/adm

STATIC_PATH=${BE_HOME_PATH}/adm-web/src/main/resources/static
JAR_PATH=${BE_HOME_PATH}/dsp-web/target/dsp-web-1.0-SNAPSHOT.jar

function prepareFE(){
    cd ${FE_HOME_PATH}
    rm -rf dist
    git pull
    rm -rf ${STATIC_PATH}/*
    cnpm run build
    cp ${FE_HOME_PATH}/dist/* ${STATIC_PATH}
}

function mavenCleanInstall(){
    cd  ${BE_HOME_PATH}
    git pull
    mvn clean install -Dmaven.test.skip=true
}

function start(){
    nohup java -jar -Xmx512m -Xms512m ${JAR_PATH}  --spring.profiles.active=test &
}

#prepareFE
mavenCleanInstall
start
